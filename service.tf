resource "kubernetes_service" "this" {
  metadata {
    name = var.name
    labels = {
      app = var.product
    }
    annotations = {
      "alb.ingress.kubernetes.io/healthcheck-path" = var.health_path
      "alb.ingress.kubernetes.io/success-codes" = var.health_check_success_code
    }
  }
  spec {
    type = "NodePort"
    selector = {
      component = var.name
    }
    port {
      protocol= "TCP"
      port        = 80
      target_port = 8080
    }
  }
  depends_on = [
    kubernetes_deployment.this
  ]
}
