locals {
  image = "${var.product}/${var.name}"
  container_name = "${var.name}-${replace(var.us_version, ".", "")}"
}
