resource "kubernetes_deployment" "this" {
  metadata {
    name = var.name
    labels = {
      app = var.product
      component = var.name
    }
    namespace = "default"
  }

  spec {
    replicas = var.replicas

    selector {
      match_labels = {
        component = var.name
      }
    }
    strategy {
      rolling_update {
        max_surge = "25%"
        max_unavailable = "25%"
      }
    }

    template {
      metadata {
        labels = {
          component = var.name
        }
      }

      spec {
        service_account_name = var.resources_service_account
        automount_service_account_token = true
        container {
          image = "${aws_ecr_repository.this.repository_url}:${var.us_version}"
          name = local.container_name
          image_pull_policy = "Always"

          volume_mount {
            name = "tz-config"
            mount_path = "/etc/localtime"
          }

          env {
            name = "AWS_REGION"
            value = var.aws_region
          }

          port {
            container_port = var.container_port
          }

          resources {
            limits {
              cpu    = var.cpu_limit
              memory = var.mem_limit
            }
            requests {
              cpu    = var.cpu_limit_request
              memory = var.mem_limit_request
            }
          }

          liveness_probe {
            http_get {
              path = var.health_path
              port = var.container_port
            }

            initial_delay_seconds = var.initial_delay
            period_seconds        = 5
          }

        }

        volume {
          name = "tz-config"
          host_path {
            path = "/usr/share/zoneinfo/America/Lima"
          }
        }

      }
    }
  }
  depends_on = [
    null_resource.deploy_image
  ]
}
