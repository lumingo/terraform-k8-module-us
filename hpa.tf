resource "kubernetes_horizontal_pod_autoscaler" "this" {
  metadata {
    name = var.name
    namespace = "default"
    labels = {
      app = var.product
    }
  }

  spec {
    min_replicas = var.min_replicas
    max_replicas = var.max_replicas
    scale_target_ref {
      kind = "Deployment"
      name = var.name
      api_version = "apps/v1"
    }

    target_cpu_utilization_percentage = var.cpu_utilization_percentage
  }
}

