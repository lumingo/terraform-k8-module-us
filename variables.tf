variable "product" {}

variable "us_version" {}

variable "name" {
  description = "nombre del servicio a deployar"
}

variable "health_path" {
    default = "/actuator/health"
}

variable "replicas" {
    default = 1
}

variable "stage" {
    default = "dev"
}

variable "namespace" {
    default = "default"
}

variable "mem_limit" {
  default = "512Mi"
}

variable "cpu_limit" {
  default = "0.5"
}

variable "container_port" {
  default = 8080
}

variable "initial_delay" {
  default = 30
}

variable "resources_service_account" {
  default = "aws-resources"
}

variable "aws_region" {
}

variable "min_replicas" {
  default = 1
}
variable "max_replicas" {
  default = 1
}
variable "cpu_utilization_percentage" {
  default = 30
}

variable "health_check_success_code" {
    default = 200
}

variable "mem_limit_request" {
  default = "512Mi"
}

variable "cpu_limit_request" {
  default = "0.25"
}
