resource "aws_ecr_repository" "this" {
  name = local.image
  tags = {
    Product = var.product
    Environment = var.stage
  }
}

resource "null_resource" "deploy_image" {

  triggers = {
    always_run = "${timestamp()}"
  }

  provisioner "local-exec" {
    environment = {
      TARGET_IMAGE = aws_ecr_repository.this.repository_url
      VERSION = var.us_version
    }
    command = "make deploy-image"
  }

  depends_on = [
    aws_ecr_repository.this
  ]
}

